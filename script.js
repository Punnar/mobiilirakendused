var bigData;


$(document).ready(function() {
	$('#movieDetails').hide();
	$('#memory').hide();

	var apiKey = 'aa7bf6278f4cec91263070ebc811d708'
	var url = 'https://api.themoviedb.org/3/movie/top_rated?api_key='+apiKey+'&language=en-US&page=1'

	
	$.get(url, function (data) {
		console.log("movies", data)
		$('#list').html('');
		for (var i = 0; i < 5; i++) {
			var currentHTML = $('#list').html();
			
			$('#list').html(currentHTML + '<div class="item" id='+i+'><img src="https://image.tmdb.org/t/p/w600/'+data.results[i].poster_path+
				'"><div class="movieInfo"><b>Title</b>: ' + 
				data.results[i].title + '<br><p><b>Year</b>: ' + data.results[i].release_date.substring(0,4) + '</p></div></div><br>');
			
		}
		bigData = data;
	});	

});


$(document.body).on('click', '#movieSearch' ,function() {
	$('#title').html("Search results");
	$('#searchButton').click(function() {
		getMovies($('#movieSearch').val());
	});
			
});



function getMovies(title) {
	$('#list').html("");
	var apiKey = 'aa7bf6278f4cec91263070ebc811d708'
	var url = 'https://api.themoviedb.org/3/search/movie?api_key='+apiKey+'&query='+title

	$.get(url, function (data) {
		console.log("movies", data)
		for (var i = 0; i < 5; i++) {
			var currentHTML = $('#list').html();
			
			$('#list').html(currentHTML + '<div class="item" id='+i+'><img src="https://image.tmdb.org/t/p/w600/'+data.results[i].poster_path+
				'"><div class="movieInfo"><b>Title</b>: ' + 
				data.results[i].title + '<br><p><b>Year</b>: ' + data.results[i].release_date.substring(0,4) + '</p></div></div><br>');
		}
		bigData = data;
		
	});
}

$(document.body).on('click', '.item' ,function() {
	console.log("vajutasid filmile");
	var id = $(this).attr('id');
	var realId = bigData.results[id].id;

	$('#mainPageContent').hide();
	$('#memory').hide();
	$('#movieDetails').show();
	$('#oneMovie').html('<img src="https://image.tmdb.org/t/p/w600/'+bigData.results[id].poster_path+
				'"><div class="movieInfo"><b>Title</b>: ' + 
				bigData.results[id].title + '<br><p><b>Year</b>: ' + bigData.results[id].release_date.substring(0,4) + '</p><br><p><b>Description</b>: ' + bigData.results[id].overview +'</p></div>');

	var currentButtonId = $('#addOrRemove').attr('id');
	var helper = 'movie' + realId;
	console.log(helper);	
	if (localStorage[helper]) {
		$('#addOrRemove').html('Remove');
	}
	$('#addOrRemove').attr('id', currentButtonId + id);
	
	
			
});


$(document.body).on('click', '#seen' ,function() {
	$('#memoryList').html('');
	$('#mainPageContent').hide();
	$('#movieDetails').hide();
	$('#memory').show();


		//-----------------------------------------------------------MEMORY
	for (var i = 0; i < localStorage.length; i++) {
		var currentHTML = $('#memoryList').html();

		$('#memoryList').html(currentHTML + localStorage.getItem(localStorage.key(i)));
	}

	
			
});

$(document.body).on('click', '[id^=addOrRemove]' ,function() {
	var id = $(this).attr('id').substr($(this).attr('id').length - 1);
	var realId = bigData.results[id].id;
	console.log("filmi päris id on: " + realId);
	//--------------------------------------------------------------------

	if (localStorage['movie' + realId]) {
		localStorage.removeItem('movie' + realId);
		$('#addOrRemove'+id).html('Add');
	} else {
		//--------------------------------------------------------------------
		
		localStorage.setItem('movie'+realId, '<div class="item" id='+id+'><img src="https://image.tmdb.org/t/p/w600/'+bigData.results[id].poster_path+
					'"><div class="movieInfo"><b>Title</b>: ' + 
					bigData.results[id].title + '<br><p><b>Year</b>: ' + bigData.results[id].release_date.substring(0,4) + '</p></div></div><br>');
		$('#addOrRemove'+id).html('Remove');
		
	
	}

	
			
});